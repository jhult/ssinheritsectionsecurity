﻿*******************************************************************************
** Component Information
*******************************************************************************

	Component:              SSInheritSectionSecurity
	Author:                 Jonathan Hult
							http://jonathanhult.com
	Last Updated On:        build_1_20121105

*******************************************************************************
** Overview
*******************************************************************************

	This component implements a SiteStudioAddNode filter. It adds security 
	information (dSecurityGroup and dDocAccount) from the parent node (if 
	there is one) to the new section.
	
	Filters:
	SiteStudioAddNode - This filter gets called during the creation of a new 
	Site Studio node (service = SS_ADD_NODE).
		
	Preference Prompts:
	SSInheritSectionSecurity_ComponentEnabled - Boolean determining if the 
	component functionality is enabled.
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	-10.1.3.5.1 (111229) (Build: 7.2.4.105)

*******************************************************************************
** HISTORY
*******************************************************************************

build_1_20121105
	- Initial component release