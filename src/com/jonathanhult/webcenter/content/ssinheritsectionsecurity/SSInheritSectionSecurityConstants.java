package com.jonathanhult.webcenter.content.ssinheritsectionsecurity;
/**
 * @author Jonathan Hult
 * {@link http://jonathanhult.com}
 */
public class SSInheritSectionSecurityConstants {
	public static final String IDC_SERVICE = "IdcService";
	public static final String SS_SET_NODES_PROPERTIES = "SS_SET_NODES_PROPERTIES";
	public static final String SS_GET_ALL_NODE_PROPERTIES = "SS_GET_ALL_NODE_PROPERTIES";
	public static final String SITE_ID = "siteId";
	public static final String NODE_ID = "nodeId";
	public static final String NEW_NODE_ID = "newNodeId";
	public static final String PROPERTY = "property";
	public static final String VALUE = "value";
	public static final String D_NAME = "dName";
	public static final String D_SECURITY_GROUP = "dSecurityGroup";
	public static final String D_DOC_ACCOUNT = "dDocAccount";
	public static final String SITE_STUDIO_PROPERTIES = "SiteStudioProperties";
	public static final String TRACE_SECTION = "ssinheritsectionsecurity";
	public static final String COMPONENT_ENABLED = "SSInheritSectionSecurity_ComponentEnabled";
}
