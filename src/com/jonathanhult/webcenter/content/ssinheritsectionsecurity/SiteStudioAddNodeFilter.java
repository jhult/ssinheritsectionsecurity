package com.jonathanhult.webcenter.content.ssinheritsectionsecurity;

import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.COMPONENT_ENABLED;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.D_DOC_ACCOUNT;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.D_NAME;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.D_SECURITY_GROUP;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.IDC_SERVICE;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.NEW_NODE_ID;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.NODE_ID;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.PROPERTY;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.SITE_ID;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.SITE_STUDIO_PROPERTIES;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.SS_GET_ALL_NODE_PROPERTIES;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.SS_SET_NODES_PROPERTIES;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.TRACE_SECTION;
import static com.jonathanhult.webcenter.content.ssinheritsectionsecurity.SSInheritSectionSecurityConstants.VALUE;
import static intradoc.shared.SharedObjects.getEnvValueAsBoolean;
import intradoc.common.ExecutionContext;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradoc.shared.FilterImplementor;
import intradoc.shared.UserData;

/**
 * @author Jonathan Hult
 * {@link http://jonathanhult.com}
 */
public class SiteStudioAddNodeFilter implements FilterImplementor {

	private static UserData m_userData;
	private static String m_userName;

	/**
	 * This method implements a SiteStudioAddNode filter for the SSInheritSectionSecurity component.
	 * First, it checks to determine whether the component is enabled by the preference prompt SSInheritSectionSecurity_ComponentEnabled.
	 * Second, it retrieves the parent section's dSecurityGroup and dDocAccount values.
	 * Third, it calls the service SS_SET_NODES_PROPERTIES.
	 * This service sets dSecurityGroup and dDocACcount from the parent node on the child node.
	 */
	public int doFilter(final Workspace ws, final DataBinder binder, final ExecutionContext ctx)
			throws DataException, ServiceException {
		traceVerbose("Start doFilter for SiteStudioAddNode for SSInheritSectionSecurity");

		try {
			// Exit if component is not enabled
			if (!getEnvValueAsBoolean(COMPONENT_ENABLED, false)) {
				traceVerbose("Component not enabled");
				return CONTINUE;
			}

			// DataBinder to execute service SS_SET_NODES_PROPERTIES
			final DataBinder serviceBinder = new DataBinder();

			// Get siteId
			final String siteId = binder.getLocal(SITE_ID);

			// Get parent nodeId
			final String parentNodeId = binder.getLocal(NODE_ID);

			// Get nodeId
			final String nodeId = binder.getLocal(NEW_NODE_ID);

			// Get all the parent node properties
			final DataResultSet allSiteProperties = getAllNodeProperties(siteId, parentNodeId, ctx);

			// Get dSecurityGroup
			final String dSecurityGroup = allSiteProperties.getStringValueByName(D_SECURITY_GROUP);

			// Get dSecurityGroup
			final String dDocAccount = allSiteProperties.getStringValueByName(D_DOC_ACCOUNT);

			// Do not call SS_SET_NODES_PROPERTIES if parent node has no security set on it
			if (dSecurityGroup == null || dSecurityGroup.trim().length() < 1 || dDocAccount == null || dDocAccount.trim().length() < 1) {
				trace("No parent node security set");
				return CONTINUE;
			}

			// Put necessary service parameters into serviceBinder
			serviceBinder.putLocal(SITE_ID, siteId);
			serviceBinder.putLocal(NODE_ID + "0", nodeId);
			serviceBinder.putLocal(NODE_ID + "1", nodeId);

			if (dSecurityGroup != null && dSecurityGroup.trim().length() > 0) {
				serviceBinder.putLocal(PROPERTY + "0", D_SECURITY_GROUP);
				serviceBinder.putLocal(VALUE + "0", dSecurityGroup);
			}

			if (dDocAccount != null && dDocAccount.trim().length() > 0) {
				serviceBinder.putLocal(PROPERTY + "1", D_DOC_ACCOUNT);
				serviceBinder.putLocal(VALUE + "1", dDocAccount);
			}

			// Execute SS_SET_NODE_PROPERTY service
			executeService(serviceBinder, (Service)ctx, SS_SET_NODES_PROPERTIES);

			return CONTINUE;
		} finally {
			traceVerbose("End doFilter for SiteStudioAddNode for SSInheritSectionSecurity");
		}
	}

	/**
	 * This method gets all the node properties for a section and returns the DataResultSet SiteStudioProperties.
	 * @param siteId The siteId of the section to get properties for.
	 * @param nodeId The nodeId of the section to get properties for.
	 * @param ctx The ExecutionContext so the SS_GET_ALL_NODE_PROPERTIES service can be called.
	 * @return DataResultSet SiteStudioProperties.
	 * @throws ServiceException
	 */
	public static DataResultSet getAllNodeProperties(final String siteId, final String nodeId, final ExecutionContext ctx) throws ServiceException {
		// DataBinder to execute service SS_GET_ALL_NODE_PROPERTIES
		DataBinder serviceBinder = new DataBinder();

		// Put necessary service parameters into serviceBinder
		serviceBinder.putLocal(SITE_ID, siteId);
		serviceBinder.putLocal(NODE_ID, nodeId);

		// Execute SS_SET_NODE_PROPERTY service
		serviceBinder = executeService(serviceBinder, (Service)ctx, SS_GET_ALL_NODE_PROPERTIES);

		// DataResultSet SiteStudioProperties
		final DataResultSet properties = new DataResultSet();
		properties.copy(serviceBinder.getResultSet(SITE_STUDIO_PROPERTIES));
		properties.first();
		traceVerbose("Parent node properties: " + properties.getCurrentRowMap().toString());
		return properties;
	}

	/**
	 * This method executes a service.
	 * @param serviceBinder The DataBinder which contains information about the service call.
	 * @param service The Service from the original service call.
	 * @param serviceName The name of the Service from to execute.
	 * @throws ServiceException
	 */
	public static DataBinder executeService(final DataBinder serviceBinder, final Service service, final String serviceName) throws ServiceException {
		traceVerbose("Start executeService");

		// Put service name to execute in serviceBinder
		serviceBinder.putLocal(IDC_SERVICE, serviceName);
		try {
			trace("Calling service " + serviceName + ": " + serviceBinder.getLocalData().toString());
			// Execute service
			service.getRequestImplementor().executeServiceTopLevelSimple(serviceBinder, serviceName, getUserData(service));
			trace("Finished calling service");
			return serviceBinder;
		} catch (final DataException e) {
			trace("Something went wrong executing service " + serviceName);
			e.printStackTrace(System.out);
			throw new ServiceException("Something went wrong executing service " + serviceName, e);
		} finally {
			traceVerbose("End executeService");
		}
	}

	/**
	 * This method retrieves the UserData from the ExecutionContext/Service.
	 * @param The Service from ExecutionContext.
	 * @return UserData for current service user.
	 */
	public static UserData getUserData(final Service service) {
		traceVerbose("Start getUserData");

		try {
			if (m_userData != null) {
				return m_userData;
			}
			// Retrieve UserData
			m_userData = service.getUserData();

			// Retrieve User name
			m_userName = m_userData.getProperty(D_NAME);
			trace("Username: " + m_userName);

			return m_userData;
		} finally {
			traceVerbose("End getUserData");
		}
	}

	/**
	 * This method retrieves the current user name from the ExecutionContext/Service.
	 * @param The Service from ExecutionContext.
	 * @return User name for current service user.
	 */
	public static String getUserName(final Service service) {
		traceVerbose("Start getUserName");

		try {
			if (m_userName != null) {
				return m_userName;
			}

			// Get UserData
			getUserData(service);

			// User name will now be available
			return m_userName;

		} finally {
			traceVerbose("End getUserName");
		}
	}

	/**
	 * This methods traces output to the WebCenter Content console.
	 * @param message The String to trace to the WebCenter Content console.
	 */
	public static void trace(final String message) {
		Report.trace(TRACE_SECTION, message, null);
	}

	/**
	 * This method verbose traces output to the WebCenter Content console.
	 * @param message The String to trace to the WebCenter Content console.
	 */
	public static void traceVerbose(final String message) {
		if (Report.m_verbose) {
			trace(message);
		}
	}
}
